#!/usr/bin/env bash

# Hangouts alternative.
brew cask install zoomus

# Slack alternative.
brew cask install mattermost

# Virtual private network client.
brew cask install pritunl

# Corporate network.
brew cask install microsoft-teams
brew cask install vmware-horizon-client

# Dell universal dock & monitor.
brew install homebrew/cask-drivers/displaylink
