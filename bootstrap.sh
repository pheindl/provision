#!/usr/bin/env bash

if [ -z "$PROVISION_USER_EMAIL" ]; then
    echo "PROVISION_USER_EMAIL required"
    exit
fi

if [ -z "$PROVISION_USER_NAME" ]; then
    echo "PROVISION_USER_NAME required"
    exit
fi

xcode-select --install

/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"

