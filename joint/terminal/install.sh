#!/usr/bin/env bash

# Terminal improvement
# brew cask install iterm2

# Timer for pomodoro
pip3 install termdown
# termdown 25m

# zsh and package manager
brew install zsh zsh-completions antigen

# Set zsh as the primary shell.
chsh -s /bin/zsh

# Copy antigen definitions over.
cp .zshrc ~/.zshrc

. ~/.zshrc