
source $(brew --prefix antigen)/share/antigen/antigen.zsh

# Load the oh-my-zsh's library.
antigen use oh-my-zsh

# Bundles from the default repo (robbyrussell's oh-my-zsh)
antigen bundle git
antigen bundle pip
antigen bundle command-not-found
antigen bundle brew
antigen bundle autojump
antigen bundle golang
antigen bundle gpg-agent
antigen bundle common-aliases
antigen bundle compleat
antigen bundle git-extras
antigen bundle git-flow
antigen bundle osx
antigen bundle web-search
antigen bundle lein

# Syntax highlighting bundle.
antigen bundle zsh-users/zsh-syntax-highlighting

# Fish-like auto suggestions
antigen bundle zsh-users/zsh-autosuggestions

# Extra zsh completions
antigen bundle zsh-users/zsh-completions

# GoBuffalo
antigen bundle 1995parham/buffalo.zsh

# Load the theme.
antigen theme robbyrussell

# Tell Antigen that you're done.
antigen apply