#!/usr/bin/env bash

# Hotkey cheatsheet
brew install cheatsheet

brew cask install google-chrome

# Resize windows with ease.
brew cask install spectacle