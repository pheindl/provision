#!/usr/bin/env bash
brew install pipenv

# Desktop app for .ipynb
brew cask install nteract
