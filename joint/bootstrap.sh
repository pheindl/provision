#!/usr/bin/env bash

source ./apps.sh

source ./go.sh

source ./ide.sh

source ./java.sh

source ./kubernetes.sh

source ./node.sh

source ./python.sh

source ./sql.sh

source ./tools.sh
