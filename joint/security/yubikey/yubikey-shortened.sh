#!/usr/bin/env bash

# Will need a more robust understanding of all of this:
# https://ttmm.io/tech/yubikey/
# https://gist.github.com/ageis/14adc308087859e199912b4c79c4aaa4
# https://www.digitalocean.com/community/tutorials/how-to-use-gpg-to-encrypt-and-sign-messages
# https://git-scm.com/book/en/v2/Git-Tools-Signing-Your-Work
# https://help.github.com/articles/associating-an-email-with-your-gpg-key/
# https://superuser.com/questions/1017749/how-do-i-manage-multiple-e-mail-addresses-with-gnupg
# https://github.com/drduh/YubiKey-Guide
# https://developers.yubico.com/yubikey-personalization/
# https://medium.com/@ahawkins/securing-my-digital-life-gpg-yubikey-ssh-on-macos-5f115cb01266
# https://florin.myip.org/blog/easy-multifactor-authentication-ssh-using-yubikey-neo-tokens
# https://evilmartians.com/chronicles/stick-with-security-yubikey-ssh-gnupg-macos
# https://support.yubico.com/support/solutions/articles/15000006478-getting-started-with-the-yubikey-on-macos
# https://code.8labs.io/help/user/project/repository/gpg_signed_commits/index.md
# https://hk.saowen.com/a/402829855c2169730010bb27f22361b5cdc644152dcd61c2fe971c823169dc98
# https://hugotunius.se/2018/07/13/yubikey-ssh-authentication.html
# https://ocramius.github.io/blog/yubikey-for-ssh-gpg-git-and-local-login/
# https://0day.work/using-a-yubikey-for-gpg-and-ssh/
# https://disjoint.ca/til/2017/10/05/a-guide-to-setting-up--managing-gpg-keys-on-a-yubikey-4/
# https://www.linode.com/docs/security/authentication/gpg-key-for-ssh-authentication/
# https://code.8labs.io/help/user/project/repository/gpg_signed_commits/index.md

To get unblocked, just creating the key on the Yubikey device.
# https://support.yubico.com/support/solutions/articles/15000006420-using-your-yubikey-with-openpgp

# Note to be very careful with the PINS. it can shut down the device.

# Yubikey personalization looks interesting but may clear settings.
# https://github.com/Yubico/yubikey-personalization


