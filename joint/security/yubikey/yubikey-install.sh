#!/usr/bin/env bash

# Command line tool built by Yubico (https://developers.yubico.com/yubikey-manager/) | 20181204
brew install ykman

# Tool to change default settings developed by Yubico (https://developers.yubico.com/yubikey-personalization/) | 20181204
brew install yubikey-personalization

