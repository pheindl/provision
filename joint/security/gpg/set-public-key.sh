#!/usr/bin/env bash


# Copy public ssh key.
gpg --export-ssh-key $(gpg --list-secret-keys --keyid-format SHORT $PROVISION_USER_EMAIL | grep sec | cut -d '/' -f 2 | cut -d ' ' -f 1) > ~/.ssh/id_rsa.pub

# Ensure gpg-agent is running to manage Yubikey ssh.
# Restart gpg-agent
gpgconf --kill gpg-agent
eval $( gpg-agent --daemon --enable-ssh-support )