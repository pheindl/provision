#!/usr/bin/env bash

# Once a key is on a Yubikey, it can not be copied again.
# So in order to have back-up Yubikeys, we need a new

# Create a temporary directory to ensure keys will be deleted.
export GNUPGHOME=$(mktemp -d) ; echo $GNUPGHOME

# Copy hardened configuration values from drduh
curl -Lfo $GNUPGHOME/gpg.conf https://raw.githubusercontent.com/drduh/config/master/gpg.conf

# Cut the wifi off at this point.

# Generate a pin for the master key. Write this down and keep safe
gpg --gen-random -a 0 24

# Create new master key
gpg --expert --full-gen-key
# - (4) RSA (sign only)
# - 4096
# - 0 (yes)

# Temporarily export the key ID for use in other processes.
# Where: gpg: key 0xFF3E7D88647EBCDB marked as ultimately trusted
export KEYID=0xFF3E7D88647EBCDB

# Edit the master to key to generate subkeys
# Consider using a 1-year expiration which can be renewed and updated.
gpg --expert --edit-key $KEYID

# Create signing key.
addkey
# - (4) RSA (sign only)
# - Valid for 1y
# - 4096

# Create encryption key.
addkey
# - (6) RSA (encrypt only)
# - Valid for 1y
# - 4096

# Create authentication key.
addkey
# - (8) RSA (set your own capabilities)
# - (S) Toggle the sign capability
# - (E) Toggle the encrypt capability
# - (A) Toggle the authenticate capability
# - (Q) Finished
# - Valid for 1y
# - 4096


# Verify keys
gpg --list-secret-keys


# Save keys
gpg --armor --export-secret-keys $KEYID > $GNUPGHOME/mastersub.key
gpg --armor --export-secret-subkeys $KEYID > $GNUPGHOME/sub.key

