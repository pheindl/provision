#!/usr/bin/env bash

# Replace the built in OSX ssh-agent with GnuPG | 20181204
brew install gnupg # Maybe avoid?

brew cask install gpg-suite

# Enable pinentry for GnuPG on macOS
brew install pinentry-mac