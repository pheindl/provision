#!/usr/bin/env bash

# Set git config data
git config --global user.name $PROVISION_USER_NAME
git config --global user.email $PROVISION_USER_EMAIL

# Find the private key created, which should have been backed up from Yubikey.
SIGNING_KEY=$(gpg --list-secret-keys --keyid-format SHORT $PROVISION_USER_EMAIL | sed -n '2p' | sed 's/ //g')

# Copy the GPG key ID that starts with sec
git config --global user.signingkey ${SIGNING_KEY}

# Tell git to include signing automatically:
git config --global commit.gpgsign true