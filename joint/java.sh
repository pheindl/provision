#!/usr/bin/env bash

brew cask install java
brew cask install caskroom/versions/java8
brew install maven

alias j11="export JAVA_HOME=`/usr/libexec/java_home -v 11.0.1`; java -version"
alias j8="export JAVA_HOME=`/usr/libexec/java_home -v 1.8`; java -version"

# export JAVA_HOME=/Library/Java/JavaVirtualMachines/openjdk-11.0.1.jdk/Contents/Home
# sudo ln -nsf $(/usr/libexec/java_home -v 1.8) $JAVA_HOME


export PATH=/usr/local/Cellar/maven/3.6.0/bin:$PATH