#!/usr/bin/env bash

brew install mariadb

brew install postgres

brew cask install teamsql
# or
brew cask install mysqlworkbench