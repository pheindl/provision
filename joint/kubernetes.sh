#!/usr/bin/env bash

# Note that Docker for Mac uses Hyperkit, rather than VirtualBox or VMWare
brew cask install docker
# Should include docker-machine, hyperkit,  docker-compose, kubernetes, etc with Docker for Mac

brew install make

brew install kubernetes-helm

# kubectl config current-context
# --> docker-for-desktop
# helm init
# This will install Tiller into the Kubernetes cluster you saw with kubectl config current-context.
# helm init --upgrade to update Tiller
