#!/usr/bin/env bash

# Good for scratch paper
brew cask install atom

brew cask install jetbrains-toolbox

brew install vim

brew cask install evernote